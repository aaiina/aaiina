﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Aaina.Data.Models;
using Aaina.Data.Repositories;
using Aaina.Dto;
using Aaina.Common;
using Newtonsoft.Json.Schema;

namespace Aaina.Service
{
    public class PlayService : IPlayService
    {
        private IRepository<Play, int> repo;
        private IRepository<PlayDelegate, int> repoDelegate;
        private IRepository<PlayPersonInvolved, int> repoInvolved;
        public PlayService(IRepository<Play, int> repo, IRepository<PlayDelegate, int> repoDelegate, IRepository<PlayPersonInvolved, int> repoInvolved)
        {
            this.repo = repo;
            this.repoDelegate = repoDelegate;
            this.repoInvolved = repoInvolved;
        }

        public PlayDto GetById(int id)
        {
            var x = this.repo.GetIncludingById(x => x.Id == id, x => x.Include(m => m.PlayPersonInvolved));
            return new PlayDto()
            {
                Name = x.Name,
                ParentId = x.ParentId,
                Id = x.Id,
                AccountableId = x.AccountableId,
                ActualEndDate = x.ActualEndDate,
                ActualStartDate = x.ActualStartDate,
                AddedOn = x.AddedOn,
                Comments = x.Comments,
                DeadlineDate = x.DeadlineDate,
                DependancyId = x.DependancyId,
                Description = x.Description,
                Emotion = !string.IsNullOrEmpty(x.Emotion) ? int.Parse(EncryptDecrypt.Decrypt(x.Emotion)) : (int?)null,
                FeedbackType = x.FeedbackType,
                GameId = x.GameId,
                IsToday = x.IsToday,
                Phoemotion = !string.IsNullOrEmpty(x.Phoemotion) ? int.Parse(EncryptDecrypt.Decrypt(x.Phoemotion)) : (int?)null,
                Priority = x.Priority,
                StartDate = x.StartDate,
                Status = x.Status,
                SubGameId = x.SubGameId,
                Type = x.Type,
                PersonInvolved = x.PlayPersonInvolved.Select(x => x.UserId).ToList()
            };
        }

        public void UpdateStatus(PlayDelegateDto dto)
        {
            var x = this.repo.Get(dto.PlayId);
            x.Status = dto.StatusId;
            x.Comments = dto.Description;
            x.ModifiedDate = DateTime.Now;

            if (dto.StatusId == (int)StatusType.Active)
            {
                x.ActualStartDate = DateTime.Now;
            }

            if (dto.StatusId == (int)StatusType.Completed)
            {
                x.ActualEndDate = DateTime.Now;
            }

            if (dto.StatusId == (int)StatusType.Delegate)
            {
                x.Status = (int)StatusType.Inactive;
                x.AccountableId = dto.DelegateId;
            }
            repo.Update(x);
            if (dto.StatusId == (int)StatusType.Delegate)
            {
                repoDelegate.Insert(new PlayDelegate()
                {
                    AccountableId = dto.AccountableId,
                    DelegateId = dto.DelegateId,
                    DelegateDate = DateTime.Now,
                    Description = dto.Description,
                    PlayId = dto.PlayId
                });
            }
        }

        public void MoveToday(List<int> ids)
        {
            var allRecord = this.repo.GetAllList(x => ids.Contains(x.Id));
            allRecord.ForEach(x =>
            {
                x.IsToday = true;
                x.ModifiedDate = DateTime.Now;
            });
            repo.UpdateRange(allRecord);

        }

        public async Task<int> AddUpdateAsync(PlayDto dto)
        {
            if (dto.Id.HasValue)
            {
                var filter = this.repo.GetIncludingById(x => x.Id == dto.Id.Value, x => x.Include(m => m.PlayPersonInvolved));

                if (filter.PlayPersonInvolved != null && filter.PlayPersonInvolved.Any())
                {
                    repoInvolved.DeleteRange(filter.PlayPersonInvolved.ToList());
                }

                filter.ParentId = dto.ParentId;
                filter.Name = dto.Name;
                filter.GameId = dto.GameId.Value;
                filter.SubGameId = dto.SubGameId;
                filter.Description = dto.Description;
                filter.AddedOn = dto.AddedOn.Value;
                filter.StartDate = dto.StartDate.Value;
                filter.DeadlineDate = dto.DeadlineDate.Value;
                filter.AccountableId = dto.AccountableId.Value;
                filter.DependancyId = dto.DependancyId;
                filter.Emotion = dto.Emotion.HasValue ? EncryptDecrypt.Encrypt(dto.Emotion.Value.ToString()) : null;
                filter.FeedbackType = dto.FeedbackType;
                filter.IsActive = dto.IsToday;
                filter.Priority = filter.Priority;
                filter.Status = filter.Status;
                filter.Type = filter.Type;
                filter.ModifiedDate = DateTime.Now;
                filter.PlayPersonInvolved = dto.PersonInvolved.Select(x => new PlayPersonInvolved()
                {
                    UserId = x,
                    PlayId = dto.Id.Value

                }).ToList();
                repo.Update(filter);
                return dto.Id.Value;
            }
            else
            {
                var filterInfo = new Play()
                {
                    CompanyId = dto.CompanyId,
                    ParentId = dto.ParentId,
                    Name = dto.Name,
                    ModifiedDate = DateTime.Now,
                    AddedDate = DateTime.Now,
                    DeadlineDate = dto.DeadlineDate.Value,
                    GameId = dto.GameId.Value,
                    SubGameId = dto.SubGameId,
                    AddedOn = dto.AddedOn.Value,
                    StartDate = dto.StartDate.Value,
                    AccountableId = dto.AccountableId.Value,
                    DependancyId = dto.DependancyId,
                    Priority = dto.Priority.Value,
                    Status = dto.Status.Value,
                    Emotion = dto.Emotion.HasValue ? EncryptDecrypt.Encrypt(dto.Emotion.Value.ToString()) : null,
                    IsActive = true,
                    Description = dto.Description,
                    FeedbackType = dto.FeedbackType,
                    Type = dto.Type,
                    PlayPersonInvolved = dto.PersonInvolved.Select(x => new PlayPersonInvolved()
                    {
                        UserId = x
                    }).ToList()
                };
                await repo.InsertAsync(filterInfo);
                return filterInfo.Id;
            }

        }

        public PreSessionAgendaDto GetPlayAction(int id)
        {
            var s = repo.GetIncludingById(x => x.Id == id, x => x.Include(m => m.Game).Include(m => m.Dependancy));
            return new PreSessionAgendaDto()
            {
                Id = s.Id,
                AccountableId = s.AccountableId,
                FeedbackType = s.FeedbackType,
                GameId = s.GameId,
                ActualEndDate = s.ActualEndDate,
                ActualStartDate = s.ActualStartDate,
                AddedOn = s.AddedOn,
                DeadlineDate = s.DeadlineDate,
                DependancyId = s.DependancyId,
                Description = s.Description,
                Name = s.Name,
                Priority = s.Priority,
                Prioritystr = ((PriorityType)s.Priority).GetEnumDescription(),
                StartDate = s.StartDate,
                Status = s.Status,
                StatusStr = ((StatusType)s.Status).GetEnumDescription(),
                SubGameId = s.SubGameId,
                Type = s.Type,
                TypeStr = ((FeedbackType)s.Type).GetEnumDescription(),
                Game = s.Game.Name,
                Dependancy = s.Dependancy != null ? s.Dependancy.Fname + " " + s.Dependancy.Lname : null,
            };
        }
        public List<SelectedItemDto> GetAllParentDrop(int companyId, int typeId, int? gId)
        {

            return repo.GetAllList(x => x.CompanyId == companyId && (gId.HasValue ? x.GameId == gId : true) && x.Type == typeId && !x.ParentId.HasValue).Select(x => new SelectedItemDto()
            {
                Name = x.Name,
                Id = x.Id.ToString()
            }).ToList();

        }

        public List<PlayDto> GetAll(int companyId, int typeId, int? gId)
        {

            return repo.GetAllList(x => x.CompanyId == companyId && (gId.HasValue ? x.GameId == gId : true) && x.Type == typeId).Select(x => new PlayDto()
            {
                Name = x.Name,
                Id = x.Id,
                AccountableId = x.AccountableId,
                ActualEndDate = x.ActualEndDate,
                ActualStartDate = x.ActualStartDate,
                AddedOn = x.AddedOn,
                Comments = x.Comments,
                DeadlineDate = x.DeadlineDate,
                DependancyId = x.DependancyId,
                Description = x.Description,
                Emotion = !string.IsNullOrEmpty(x.Emotion) ? int.Parse(EncryptDecrypt.Decrypt(x.Emotion)) : (double?)null,
                FeedbackType = x.FeedbackType,
                GameId = x.GameId,
                IsToday = x.IsToday,
                Phoemotion = !string.IsNullOrEmpty(x.Phoemotion) ? int.Parse(EncryptDecrypt.Decrypt(x.Phoemotion)) : (double?)null,
                Priority = x.Priority,
                StartDate = x.StartDate,
                Status = x.Status,
                SubGameId = x.SubGameId,
                Type = x.Type
            }).ToList();

        }

        public List<PlayDto> GetAll(int companyId, int typeId, bool istoday, int? userId, int? gId)
        {
            var parentList = repo.GetAllIncluding(x => x.CompanyId == companyId && (gId.HasValue ? x.GameId == gId : true) && (!istoday ? x.Type == typeId : true) && (userId.HasValue ? x.AccountableId == userId || x.PlayPersonInvolved.Any(a => a.UserId == userId) : true) && x.IsToday == istoday && x.Status != (int)StatusType.Completed, x => x.Include(m => m.Game).Include(m => m.SubGame)
                              .Include(m => m.Accountable).Include(m => m.Dependancy).Include(m => m.PlayPersonInvolved).Include("PlayPersonInvolved.User")).Select(x => new PlayDto()
                              {
                                  Name = x.Name,
                                  ParentId = x.ParentId,
                                  Id = x.Id,
                                  AccountableId = x.AccountableId,
                                  Accountable = x.Accountable.Fname + " " + x.Accountable.Lname,
                                  Dependancy = x.Dependancy != null ? x.Dependancy.Fname + " " + x.Dependancy.Lname : null,
                                  ActualEndDate = x.ActualEndDate,
                                  ActualStartDate = x.ActualStartDate,
                                  AddedOn = x.AddedOn,
                                  Comments = x.Comments,
                                  DeadlineDate = x.DeadlineDate,
                                  DependancyId = x.DependancyId,
                                  Description = x.Description,
                                  Emotion = !string.IsNullOrEmpty(x.Emotion) ? int.Parse(EncryptDecrypt.Decrypt(x.Emotion)) : (int?)null,
                                  FeedbackType = x.FeedbackType,
                                  GameId = x.GameId,
                                  Game = x.Game.Name,
                                  SubGame = x.SubGame != null ? x.SubGame.Name : string.Empty,
                                  IsToday = x.IsToday,
                                  Phoemotion = !string.IsNullOrEmpty(x.Phoemotion) ? int.Parse(EncryptDecrypt.Decrypt(x.Phoemotion)) : (int?)null,
                                  Priority = x.Priority,
                                  StartDate = x.StartDate,
                                  Status = x.Status,
                                  SubGameId = x.SubGameId,
                                  Type = x.Type,
                                  PersonInvolvedStr = x.PlayPersonInvolved.Any() ? string.Join(",", x.PlayPersonInvolved.Select(x => x.User.Fname + " " + x.User.Lname).ToList()) : string.Empty
                              }).ToList();


            var palyList = parentList.Where(a => !a.ParentId.HasValue || (a.ParentId.HasValue && !parentList.Any(q => q.ParentId == a.Id))).ToList();
            palyList.ForEach(a =>
            {
                a.ChildList = parentList.Where(w => w.ParentId == a.Id).ToList();
            });

            return palyList;

        }

        public async Task<bool> IsExist(int companyId, string name, int? id, int typeId)
        {
            var result = await this.repo.CountAsync(x => x.CompanyId == companyId && x.Type == typeId && x.Id != id && x.Name == name);
            return result > 0;
        }

        public async Task Delete(int id)
        {
            var context = this.repo.GetContext();
            try
            {
                context.Database.BeginTransaction();
                var filter = this.repo.GetIncludingById(x => x.Id == id, x => x.Include(m => m.PlayPersonInvolved).Include(m => m.PlayDelegate));
                if (filter.PlayPersonInvolved != null && filter.PlayPersonInvolved.Any())
                {
                    repoInvolved.DeleteRange(filter.PlayPersonInvolved.ToList());
                }
                if (filter.PlayDelegate != null && filter.PlayDelegate.Any())
                {
                    repoDelegate.DeleteRange(filter.PlayDelegate.ToList());
                }
                await this.repo.DeleteAsync(filter);
                context.Database.CommitTransaction();
            }
            catch (Exception ex)
            {
                context.Database.RollbackTransaction();
                throw;
            }

        }

    }
}
