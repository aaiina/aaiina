﻿(function ($) {
    function FilterAddEdit() {
        var $this = this, form;
        var validationSettings;

        function initilizeModel() {

            form = new Global.FormHelper($("#frm-add-edit-play").find("form"), { updateTargetId: "validation-summary", validateSettings: validationSettings }, null, null);

            $('.datepicker').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                toggleActive: false,
                autoclose: true,
                format: 'dd/mm/yyyy'
            }).inputmask("dd/mm/yyyy", { "placeholder": "dd/mm/yyyy" });


            $('#StartDate').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                toggleActive: false,
                autoclose: true,
                format: 'dd/mm/yyyy'
            }).on('changeDate', function (selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#DeadlineDate').datepicker('setStartDate', minDate);
            }).inputmask("dd/mm/yyyy", { "placeholder": "dd/mm/yyyy" });

            $('#DeadlineDate').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                toggleActive: false,
                autoclose: true,
                format: 'dd/mm/yyyy'
            }).on('changeDate', function (selected) {
                var maxDate = new Date(selected.date.valueOf());
                $('#StartDate').datepicker('setEndDate', new Date(maxDate));
            }).inputmask("dd/mm/yyyy", { "placeholder": "dd/mm/yyyy" });



            if ($('#DeadlineDate').val() != "") {
                var minDate = new Date(moment($("#DeadlineDate").val(), "DD/MM/YYYY"));
                $('#StartDate').datepicker('setEndDate', minDate);
            }

            if ($('#StartDate').val() != "") {
                var minDate = new Date(moment($("#StartDate").val(), "DD/MM/YYYY"));
                $('#DeadlineDate').datepicker('setStartDate', minDate);
            }

            let emojifeedback = Emotion;
            var newValue = Number((emojifeedback - 1) * 100 / (10 - 1))
            var newPosition = 10 - (emojifeedback * 5.5);

            $("#view_emotion").attr('style', `left:calc(${newValue}% + (${newPosition}px))`);



            $("#PersonInvolved").select2();

            $("#GameId").on('change', function () {
                var forId = $("#SubGameId");
                forId.empty();
                $('<option/>', {
                    'value': '',
                    'html': 'Select'
                }).appendTo(forId)

                if (parseInt($(this).val()) > 0) {
                    var url = $(this).data('url') + $(this).val();

                    $.get(url, function (response) {
                        for (var i = 0; i < response.length; i++) {
                            var item = response[i];
                            $('<option/>', {
                                'value': item.id,
                                'html': item.name
                            }).appendTo(forId)
                        }
                    })
                }
            });


            $(document).on('click', '.range_change', function () {

                var range = document.getElementById('Emotion'),
                    rangeV = document.getElementById('view_emotion');
                const
                    newValue = Number((range.value - range.min) * 100 / (range.max - range.min)),
                    newPosition = 10 - (range.value * 5.5);
                rangeV.innerHTML = `<span><img src="${Global.GetEmojiName(range.value)}" class="imgemoji" /></span>`;
                rangeV.style.left = `calc(${newValue}% + (${newPosition}px))`;

            });
        }


        $this.init = function () {
            validationSettings = {
                ignore: '.ignore'
            };

            initilizeModel();
        }
    }

    $(function () {
        var self = new FilterAddEdit();
        self.init();
    })
})(jQuery)