﻿(function ($) {
    function ActionPlayIndex() {
        var $this = this, form;
        var validationSettings;

        function initilizeModel() {
            $('.all_action_check').change(function () {

                $(".action_check").prop('checked', $(this).is(":checked"))

            });

            $(".action_play_status").on('change', function () {
                var statusId = $(this).val();
                var status = $(this).find("option:selected").text();
                var id = $(this).data('id');
                var text = $(this).data('text');
                var accountable = $(this).data('accountable');
                var accountableid = $(this).data('accountableid');

                $("#sub_title").html(`Change status ${text} to ${status}`);
                $("#playid").val(id);
                $("#playstatus").val(statusId);
                $("#accountableid").val(accountableid);
                $("#delegate_label").html(`Delegate from ${accountable} to`);
                $("#comments_status").val("");
                $("#delegate_status").val("");


                if (parseInt(statusId) == 2) {
                    $("#div_delegate").show();
                } else {
                    $("#div_delegate").hide();
                }

                $("#modal-add-edit-status").modal('show');

            });

            $(document).on('click', '#btnsubmit', function () {
                var url = $(this).data('url');
                var playid = $("#playid").val();
                var playstatus = $("#playstatus").val();
                var accountableid = $("#accountableid").val();
                var comments = $("#comments_status").val();
                var delegate = $("#delegate_status").val();

                if (comments == "") {
                    alert('comment is required');
                }

                if (parseInt(playstatus) == 2 && delegate == "") {
                    alert('Select delegate user');
                }

                if ((parseInt(playstatus) == 2 && delegate != "") || (parseInt(playstatus) != 2 && comments != "")) {
                    var data = {
                        PlayId: playid,
                        StatusId: playstatus,
                        AccountableId: accountableid,
                        DelegateId: delegate,
                        Description: comments
                    };
                    UpdateData(url, data)
                }


            });

            $(".buttons-excel").on('click', function () {
                var url = $(this).data('url');
                window.location.href = url;
            });

            $("#btn_add_today").on('click', function () {
                var url = $(this).data('url');
                var favorite = [];
                $.each($(".action_check:checked"), function () {
                    favorite.push($(this).val());

                });

                if (favorite.length > 0) {
                    UpdateData(url, { id: favorite.join(",") })
                }


            });

            Global.ModelHelper($("#modal-action-delete-play"), function () {
                form = new Global.FormHelper($("#modal-action-delete-play").find("form"), {
                    updateTargetId: "validation-summary",
                    refreshGrid: false,
                    modelId: 'modal-action-delete-game'
                }, null, null);

            }, null);

            $(".buttons-excel").on('click', function () {
                var url = $(this).data('url');
                //window.location.href = url;
                var id = $(this).data('id');
                var $html = $("#" + id).html();
                $html = $html.replace('display: none;', '');
                var newHtml = '<html><head><title>PDF Export</title></head><body >' + $html + '</body></html>';
                Global.Excel(newHtml);
            });

            $(".buttons-pdf").on('click', function () {
                var id = $(this).data('id');
                var $html = $("#" + id).html();
                $html = $html.replace('display: none;', '');
                var newHtml = '<html><head><title>PDF Export</title></head><body >' + $html + '</body></html>';
                Global.Pdf(newHtml);
            });

            $(".buttons-print").on('click', function () {
                var id = $(this).data('id');
                var $html = $("#" + id).html();
                Global.Print($html, "action-play");
            });

            $(".gameselect").on('change', function () {
                var url = $(this).data('url');
                var gId = $(this).val();
                url = '/'+gId+url;
                window.location.href = url;

            })
            $("#filter_user").on('change', function () {
                var uId = $(this).val();
                var url = window.location.href;            
                var nreUrl = Global.AddUpdateQueryStringParameter(url, 'uid', uId);
                window.location.href = nreUrl;

            })

            

        }

        function UpdateData(url, dataRequest) {
            Global.ShowLoading();
            $.ajax(url, {
                type: "POST",
                data: dataRequest,
                success: function (result) {
                    window.location.reload();
                },
                error: function (jqXHR, status, error) {

                    if (onError !== null && onError !== undefined) {
                        onError(jqXHR, status, error);
                    } else {


                    }
                    Global.HideLoading();
                }, complete: function () {
                    Global.HideLoading();
                }
            });
        }


        $this.init = function () {
            validationSettings = {
                ignore: '.ignore'
            };

            initilizeModel();
        }
    }

    $(function () {
        var self = new ActionPlayIndex();
        self.init();
    })
})(jQuery)